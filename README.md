### Getting up and running

1. Run `npm install` from the root directory
2. Run `npm run dev`
3. Your browser will automatically be opened and directed to the browser-sync proxy address
4. To prepare assets for production, run the `npm run build` script (Note: the production task does not fire up the express server, and won't provide you with browser-sync's live reloading. Simply use `npm run dev` during development. More information below)

Now that `npm run dev` is running, the server is up as well and serving files from the `/build` directory. Any changes in the `/app` directory will be automatically processed by Gulp and the changes will be injected to any open browsers pointed at the proxy address.

### What I would improve

1. Extracting the texts for all of the images and the sprites in similar way to the "Join now" CTA button
2. Applying the right fonts
3. Creating separate Angular modules for the footer and the navigation in order to maintain them separately from the main part of the app 