function ExampleCtrl() {

  // ViewModel
  const vm = this;

  vm.moreInfo = false;

  vm.join = function () {
    alert('Woudl you like to join?');
  }

  vm.showMoreInfo = function() {
    vm.moreInfo = ! vm.moreInfo;
  }

}

export default {
  name: 'ExampleCtrl',
  fn: ExampleCtrl
};
